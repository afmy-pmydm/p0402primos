package net.iescierva.dam20_18.p0402primos;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG ="p0402primos" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = findViewById(R.id.button);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.d(TAG,"entrando en OnCreate");
        TextView numero=findViewById(R.id.editTextNumero);
        TextView resultado=findViewById(R.id.textViewResultado);
        Locale l=Locale.getDefault();

        String s=numero.getText().toString();
        int n=Integer.parseInt(s);

        PrimeNumbersCollection c;
        try {
            c = new PrimeNumbersCollection(n);
            resultado.setText(String.format(l,"%d",c.get(n) ));
        } catch (Exception e) {
            resultado.setText(getResources().getText(R.string.excepcion));
        }  // finally {}

        Log.d(TAG,"saliendo de OnCreate");

    }
}